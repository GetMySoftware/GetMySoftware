# Cahier des charges

## Composants

GetMySoftware est constitué de deux composants principaux :

* le module de mise à jour de catalogue,
* le module de téléchargement de catalogue.

## Vocabulaire

### Catalogue

Un catalogue est une description de logiciels sous forme de fichier texte. Il présente a minima les informations suivantes :

* nom du logiciel
* URL du logiciel
* URL de téléchargement du logiciel
* numéro de la dernière version
* numéro de l'avant-dernière version
* date de mise à jour du catalogue

### Signature

Un fichier est signé afin de garantir son authenticité.

## Module de mise à jour de catalogue

* Entrée : catalogue
* Sortie : catalogue avec les dernières versions des logiciels

## Module de téléchargement

* Entrée : catalogue
* Sortie :
    * ensemble de fichiers, signés, correspondant aux logiciels du catalogue
    * fichier de log indiquant les éventuelles erreurs de téléchargement ou signature

Le nom des fichiers téléchargés doit être configurable selon un modèle de nommage. Exemples de modèles de nom :

* `<nomLogiciel>-<version>-<architecture>.<extension>`
* `<nomLogiciel>-<date>-<plateforme>.<extension>`

## Autres informations

* Le projet GetMySoftware doit fournir un **exemple** de catalogue. Les catalogues partagés par des utilisateur seront placés dans un autre repos à définir.
* Afin de tester différents cas, il peut être intéressant de tester tous les [logiciels du SILL 2017](http://eole.ac-dijon.fr/ethercalc/mimo2017)