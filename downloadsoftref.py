#!/usr/bin/python
import os, re, sys, requests
from urllib2  import urlopen, URLError, HTTPError, Request
import urllib2
import ssl
from bs4 import BeautifulSoup
from datetime import datetime

# probleme a resoudre :
#   gestion crlf dans stringversion
#   

# formats de date gere :
#Jan[uary] 11[,] 2004
#jj[/.-]mm[/.-]aaaa
#aaaa-mm-jj
#mm-jj-aaaa

#, re.IGNORECASE)
#^\d{4}[-/\.]\d{2}[-/\.]\d{2}$|^\d{2}[-/\.]\d{2}[-/\.]\d{4}|
#(Jan|January|Feb|February|Mar|March|Apr|April|May|Jun|June|Jul|July|Aug|August|Sep|September|Oct|October|Nov|November|Dec|December)( )\d{2}((, )|( ))\d{4}

regexdate = '(\d{2}[-\/.]\d{2}[-\/.]\d{4}|\d{4}[-\/.]\d{2}[-\/.]\d{2}|(Jan|January|Feb|February|Mar|March|Apr|April|May|Jun|June|Jul|July|Aug|August|Sep|September|Oct|October|Nov|November|Dec|December)( )\d{2}((, )|( ))\d{4})'

strselect = '(\d{4}[ ][sS][pP]\d)|(\d+[.]\d+[.]\d+[.]\d+)|(\d+[.]\d+[.]\d+)|(\d+[.]\d+)'

# Exemple des 3 types de numerotation des versions geres :
# 0.91                    ]
# 45.2.0                  ] ---> (\d+[.]\d+[.]\d+[.]\d+)|(\d+[.]\d+[.]\d+)|(\d+[.]\d+)
# 21.0.0.242              ]
# 2016 SP1                ] pour draftsight (\d{4}[ ][sS][pP]\d)

if os.path.isfile("downloadsoftref.log"):		# initialisation du log avant chaque mise a jour
	os.remove("downloadsoftref.log")

class Logger(object):					# redirige les sorties sys.stdout et print vers un fichier de log
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

sys.stdout = Logger("downloadsoftref.log")			# capture de la sortie ecran dans le fichier de log

# gestion du proxy
varproxy = {'http': '172.16.47.1:8080','https': '172.16.47.1:8080'}# gestion protocole securise

urllib2.install_opener(
    urllib2.build_opener(
        urllib2.ProxyHandler({'http': '172.16.47.1:8080','https': '172.16.47.1:8080'})
    )
)
# gestion protocole securise

ssl._create_defaut_https_context = ssl._create_unverified_context  # pour eviter HTTP error : 503

#--------------------------------------------------------------------------------------
def recupdate(strdate):
    try:
        if url_date!='':
           print('fonction recupdate :',url_date)
           req = requests.get(url_date,proxies=varproxy,verify=True)
           latestdate = ''
           countline = 0
           for line in req:
              countline+=1
              if "soup.find" in strdate:		# recherche objet html avec le parser beautifulsoup
                 result = requests.get(url_date,proxies=varproxy,verify=True)
                 contenu = result.content
                 soup = BeautifulSoup(contenu)
                 expression = eval(strdate)
                 print ('recupdate expression :', expression)
                 if "numline" in strdate:
                    if countline == int(strdate[strdate.find("numline")+7:]):
                        hdlatestdate = re.search(regexdate,line, flags=re.IGNORECASE)
                        if hdlatestdate >0:
                           latestdate = hdlastestdate.group()
                           return latestdate
#                        else:
#                           break
           if strdate in line:
              print ('strdate in line :',line[line.find(strdate):])
              hdlatestdate = re.search(regexdate,line[line.find(strdate):], flags=re.IGNORECASE)
              if hdlatestdate > 0:
                 latestdate = hdlatestdate.group()
                 return latestdate
#                 break
              else: # la recherche est faite sur toute la ligne
                    # '2ieme methode recherche'
                 hdlatestdate = re.search(regexdate,line)
                 if hdlatestdate > 0:
                    print (hdlatestdate.group())
                    latestdate = hdlatestdate.group()
                    return latestdate
#                 else:
#                    break
           else:
              latestdate=''		
        if latestdate == '':
           return 'None'
        url.close()
    except HTTPError as e:
        if e.reason == 'Forbidden':
           req = Requests.get(string_url, headers={'User-Agent' : "Magic Browser"})   # ou "Mozilla/5.0" pour eviter -> HTTP Error: Forbidden
        else:
           print ("HTTP Error:",e.reason)
    except URLError as e:
        print ("URL Error:",e.reason)
        

#--------------------------------------------------------------------------------
def recupversion(strvers,strurl):
    try:
        req = requests.get(string_url,proxies=varproxy,verify=True)
        latestvers=""
        countline = 0
        trouve = 0
        for line in req:
           countline +=1
           if "numline" in strvers:		# pointer un numero de ligne avec le mot cle 'numline' dans stringversion
              if countline == int(strvers[strvers.find("numline")+7:]):	# extraction et convertion numero ligne
                 print ('strvers :',strvers,line)

                 hdlatestvers = re.search(strselect,line)			# recuperation version dans la ligne designee
                 if hdlatestvers > 0:
                    latestvers = hdlatestvers.group()
                    return latestvers
                    break

           if "soup.find" in strvers:		# recherche objet html avec le parser beautifulsoup
              result = requests.get(string_url,proxies=varproxy,verify=True)
              contenu = result.content
              soup = BeautifulSoup(contenu)
              expression = eval(strvers)
              hdlatestvers = re.search(strselect,str(expression))
              if hdlatestvers >0:
                 latestvers = hdlatestvers.group()
                 return latestvers
           if strvers in line:
              hdlatestvers = re.search(strselect,line[line.find(strvers):])
              if hdlatestvers > 0:
                 trouve = 1
                 latestvers = hdlatestvers.group()
                 return latestvers
              else: # la recherche est faite sur toute la ligne
                    # '2ieme methode recherche'
                 hdlatestvers = re.search(strselect,line)
                 if hdlatestvers > 0:
                    latestvers = hdlatestvers.group()
                    return latestvers
	    	    
        if latestvers=="":
           return "None"     
        url.close()
    except HTTPError as e:
        if e.reason == 'Forbidden':
           req = Requests.get(string_url, headers={'User-Agent' : "Magic Browser"})   # ou "Mozilla/5.0" pour eviter -> HTTP Error: Forbidden
        else:
           print ("HTTP Error:",e.reason)
    except URLError as e:
        print ("URL Error:",e.reason)

#-------------------------------------------------------------------------------
def dnlfile(url):
    # Open the url
    try:
        f = urlopen(url)
        print ("downloading " + url + "\nfichier local paquet-appli/"+filename)
	#localfile=os.path.basename("paquet-appli/"+filename)
        # Open our local file for writing
        with open("paquet-appli/"+filename, "wb") as local_file:
            local_file.write(f.read())
        cfgfile = open("downloadsoftref.ini",'w+')
        config.set(section, 'latestfile',filename)
        config.write(cfgfile)
        cfgfile.close()
    #handle errors
    except HTTPError as e:
        if e.reason == 'Forbidden':
#           url.close()
           req = urllib2.Request(string_url, headers={'User-Agent' : "Magic Browser"})     # ou Mozilla/5.0  pour eviter -> HTTP Error: Forbidden
           url = urllib2.urlopen( req )
        else:
           print ("HTTP Error:",e.reason)
           cfgfile = open("downloadsoftref.ini",'w+')
           config.set(section, 'latestfile',e.reason)
           config.write(cfgfile)
           cfgfile.close()

    except URLError as e:
        print ("URL Error:", e.reason, url)
        cfgfile = open("downloadsoftref.ini",'w+')
        config.set(section, 'latestfile',e.reason)
        config.write(cfgfile)
        cfgfile.close()

#-----------------------------------------------------------------------------
# 			programme principal
#-----------------------------------------------------------------------------

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

config = ConfigParser()
config.read('downloadsoftref.ini')
listsections = config.sections()  # recuperation liste des sections

for section in listsections:
    string_url = config.get(section, 'URLversion')
    url_date = config.get(section, 'URLdate')
    string_version = config.get(section, 'stringversion')
    string_date = config.get(section, 'stringdate')
    latestverini = config.get(section, 'latestversion')
    oldversion = config.get(section, 'oldversion')
    latestfile = config.get(section, 'latestfile')
    print (section)
#    print ("string_version :",string_version)
    latestversion = recupversion(string_version,string_url)  # recuperation de numero de la derniere version
    print ("latestversion:",latestversion)
    if latestversion == "None":				# si on recupere un numero de version
       print ("chaine version vide!")
#	continue
    else:
        if string_date.find('{version}') and latestversion != "None" and string_date != '':
           string_date = string_date.replace('{version}',latestversion)
           latestdate = recupdate(string_date)
#           latestdate = datetime.strptime(latestdate, '%b %d %Y')
#           latestdate = latestdate.strftime ('%d/%m/%y')
           print ('date version :', latestdate)
           config.set(section, 'latestdate',latestdate)
        filename=''		# pour ne pas sortir le nom de fichier du logiciel si on ne trouve pas de version
#	print ("chaine version non vide")
        genfilename = config.get(section, 'filename')                              # traitement du nom generique
        if genfilename.find('{version}') and latestversion != "None" :                                          # numero de version entier a mettre
           if latestversion:
              filename = genfilename.replace('{version}',latestversion)              # fabrication du nom de fichier a telecharger
              if not os.path.isfile("paquet-appli/"+genfilename.replace('{version}',latestversion)): 	# verification de l'existence de ce fichier
                 print ('PAS DE FICHIER :',genfilename.replace('{version}',latestversion))
                 urldownloadgen = config.get(section, 'URLdownload')
                 if urldownloadgen != "":
                    posregex = urldownloadgen.find("re.search")             # gestion extrait version dans URL : expression reguliere python evaluee dans URL
                    version = latestversion					# parce que la variable version se trouve dans 're.search'
                    if posregex > 0:
                       debre = urldownloadgen[posregex:]
                       finchaine = urldownloadgen[debre.find('}')+int(posregex)+1:]
                       expression = eval(debre[:debre.find('}')])  	# position fin cmd regex
                       calculexpression = expression.group()   		# resultat evaluation regex
                       debutchaine = urldownloadgen[:int(posregex)-1]
                       urldownloadgen = debutchaine+calculexpression+finchaine
                       posreplace = urldownloadgen.find("version.replace")	# gestion delimiteur change dans version : methode 'replace' python evaluee dans URL
                       version = latestversion                          # parce que la variable version se trouve dans 'version.replace'
                       while posreplace > 0:
                          if posreplace > 0:
                             debrep = urldownloadgen[posreplace:]
                             finchaine = urldownloadgen[debrep.find('}')+int(posreplace)+1:]
                             expression = eval(debrep[:debrep.find('}')])
                             debutchaine = urldownloadgen[:int(posreplace)-1]
                             urldownloadgen = debutchaine+expression+finchaine
                             posreplace = urldownloadgen.find("version.replace")
                          if strdate.find('{version}'):
                             chainedate = strdate.replace('{version}',latestversion)
                             recupdate(chainedate)
                          if urldownloadgen.find('{version}'):
                             chemintelechargement = urldownloadgen.replace('{version}',latestversion)
                             urldownloadgen = chemintelechargement
                             print ('chemintelechargement :',chemintelechargement)
                       dnlfile (urldownloadgen)
                 else:
                    print ('le fichier',filename,'existe localement')
        cfgfile = open("downloadsoftref.ini",'w+') 			# mise a jour du fichier ini
#        if oldversion == "":	# si pas ancienne version mettre la nouvelle version connue
#           config.set(section, 'oldversion',latestversion)
        if latestverini == "":	# si pas nouvelle version mettre la nouvelle version connue
           config.set(section, 'latestversion',latestversion)
        if latestverini != latestversion: # si nouvelle version trouve mettre la precedente dans old
           config.set(section, 'oldversion',latestverini)
           config.set(section, 'latestversion',latestversion)	# la nouvelle dans latestversion
        if latestfile =="":
           config.set(section, 'latestfile',filename)
        config.write(cfgfile)
        cfgfile.close()
